package com.hotmail.fabiansandberg98;

import java.net.MalformedURLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	/**
	 * @TODO CHANGE MAIN TO YOUR CLASS NAME!
	 */
private Main plugin;
	
	public Boolean newupdate;
	
	// Enable/Disable updater
	public Boolean updaterStatus;
	
	public UpdaterClass updateUrl = new UpdaterClass();
	public String latestversion;
	
	@Override
	public void onDisable() {
		getLogger().info("[" + plugin.getName() + "] starting onDisable!");
		plugin = this;
		
		// Cancel the update-check scheduler
		Bukkit.getScheduler().cancelTasks(plugin);
		
		// All done!
		getLogger().info("[" + plugin.getName() + "] has been disabled!");
	}
	
	@Override
	public void onEnable() {
		getLogger().info("[" + plugin.getName() + "] starting onEnable!");
		plugin = this;
		
		// "UpdaterStatus"
		getConfig().addDefault("update-check", true);
		plugin.updaterStatus = getConfig().getBoolean("update-check");
		
		// Back to configuration..
        getConfig().options().copyDefaults(true);
        saveConfig();
		
		// Register event
        plugin.getServer().getPluginManager().registerEvents(new UpdateListener(plugin), plugin);
        
        /*
         * Time to check for updates ;)
         */
        
        if (plugin.updaterStatus) {
        
        this.getServer().getScheduler().runTask(plugin, new Runnable() {

            @Override
            public void run() {
                org.bukkit.permissions.Permission perm = getServer().getPluginManager().getPermission(plugin.getName() + ".update");
                if (perm == null)
                {
                    perm = new org.bukkit.permissions.Permission(plugin.getName() + ".update");
                    perm.setDefault(PermissionDefault.OP);
                    plugin.getServer().getPluginManager().addPermission(perm);
                }
                perm.setDescription("Allows a user or the console to check for updates");

                getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {

                    @Override
                    public void run() {
                        if (getServer().getConsoleSender().hasPermission(plugin.getName() + ".update") && getConfig().getBoolean("update-check", true)) {
                            try {
                            	getLogger().info("Checking for Updates ... ");
                                UpdaterThread check = new UpdaterThread(plugin);
                                check.start();
                            } catch (Exception e) {
                                // ignore exceptions
                            }
                        }
                    }
                }, 0, 36000);

            }

        });
        
        } else {
        	getLogger().info("[" + plugin.getName() + "] Updater is disabled!");
        }
        
		// All done!
		getLogger().info("[" + plugin.getName() + "] has been enabled!");
	}
	
	/*
	 * Updater Class
	 */
	
	private class Updater implements Runnable {
		private String message;

		private Updater(String message) {
			this.message = message;
		}

		public void run() {
			plugin.getLogger().info("[" + plugin.getName() + "] " + this.message);
		}
	}

	private class UpdaterThread extends Thread {
		
		/**
		 * @TODO CHANGE MAIN TO YOUR CLASS NAME!
		 */
		private Main plugin;

		/**
		 * @TODO CHANGE MAIN TO YOUR CLASS NAME!
		 */
		private UpdaterThread(Main plugin) {
			this.plugin = plugin;
		}

		@SuppressWarnings("deprecation")
		public void run() {
			try {
				plugin.latestversion = plugin.updateUrl.getLatestVersion();
				if (plugin.latestversion == null) {
					/*Removed message
					*plugin.getLogger().info("[Updater] No new versions was found!);
					*/
					
				} else if (plugin.updateUrl.compareVersion(
						plugin.latestversion, plugin.getDescription()
								.getVersion())) {
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Updater("New version of " + plugin.getName() + " available: " + plugin.latestversion + ". \nYou have version " + plugin.getDescription().getVersion()), 0L);
					plugin.newupdate = true;
				}
			} catch (MalformedURLException mue) {
				// ignore exceptions
			}
			stop();
		}
	}
	
	/*
	 * Easy and stupid Booleans
	 */
	public boolean hasPerms(Player player, String perm) {
		return player.hasPermission(perm);
	}
	
	/*
	 * Update Listener will look for an update every 30 minutes!
	 * This class will handle update message on login
	 */
	public class UpdateListener implements Listener {
		/**
		 * @TODO CHANGE MAIN TO YOUR CLASS NAME!
		 */
		Main plugin;
		
		/**
		 * @TODO CHANGE MAIN TO YOUR CLASS NAME!
		 */
		public UpdateListener (Main plugin) {
			this.plugin = plugin;
		}

        @EventHandler(priority = EventPriority.MONITOR)
        public void onPlayerJoin(PlayerJoinEvent event) {
            Player player = event.getPlayer();
            if (plugin.hasPerms(player, plugin.getName() + ".update")) {
            	
            	if (plugin.updaterStatus) {
            		if (plugin.newupdate) {
            			player.sendMessage(ChatColor.RED + "New version of " + plugin.getName() + " available: " + plugin.latestversion + ". \nYou got version " + plugin.getDescription().getVersion());
            		}
            		
            	} //Don't add a message here, it's just annoying
            } //The same counts for this line!
        }
	}
}

