package com.hotmail.fabiansandberg98;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class UpdaterClass {
	public String getLatestVersion() throws MalformedURLException {
		String version = null;
		try {
			
			// Example of a raw file: http://pastebin.com/raw.php?i=XNbEgnzZ
			
			/*
			 * If you don't know how to get a raw file, then read this..
			 * 1. Go to www.pastebin.com and register
			 * 2. Click "Create a new paste", and type it whatever you need
			 * 3. Then you click submit (Don't submit it as Private!)
			 * 4. When you go to your paste, copy the paste id. You will find it on the link (http://pastebin.com/[This is the id: XNbEgnzZ])
			 * 5. This is one way to get the raw file, add the id after this: http://pastebin.com/raw.php?i= (It should look like this when you do it: http://pastebin.com/raw.php?i=XNbEgnzZ)
			 * 6. Copy the link and paste it in the URL below. Thats one way.
			 */
			
			URL url = new URL(
					"Paste Raw File Link Here");
			URLConnection urlconnect = url.openConnection();
			BufferedReader bufferedreader = new BufferedReader(
					new InputStreamReader(urlconnect.getInputStream()));

			boolean gotver = false;
			String l;
			while (((l = bufferedreader.readLine()) != null) && (!gotver)) {

				String[] tokens = l.split("[:]");
				if (tokens.length == 2) {
					// Paste the string from your raw file here. It should look like this: "myplugin: 1.0".
					if (tokens[0].equalsIgnoreCase("StringFromTextFile")) {
						version = tokens[1].trim();
						gotver = true;
					}
				}
			}
			bufferedreader.close();
		} catch (IOException e) {
			return null;
		}
		return version;
	}

	public boolean compareVersion(String newversion, String oldversion) {
		if (newversion.equals(oldversion)) {
			return false;
		}
		return true;
	}
}